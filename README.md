# airflow

### What is Airflow?
Airflow is an open-source workflow management platform. Read the [tutorial](https://airflow.apache.org/docs/stable/tutorial.html) here.

Use this repo to version control Apache Airflow DAGs

### Installation (FreeBSD)

```
# log into staging -> ssh <user>@172.31.82.178

cd /home/vkofia

# set up python  
python -m venv airflow-env  
source ./airflow-env/bin/activate  
cd airflow-env  
  
#install using pip  
python -m pip install apache-airflow  
#sudo apt-get install python-mysqldb  
python -m pip install mysqlclient  
export LANG='en_US.UTF-8'  
  
# initialize the database (uses sqlite as backend by default)  
airflow initdb  
  
# start the web server on port 1313, default port is 8080  
airflow webserver -D -p 1313
  
# start the scheduler (as a daemon)  
airflow scheduler -D
  
# visit localhost:1313 in the browser and enable the example dag in the home page  
  
#refresh dags  
python -c 'from airflow.models import DagBag; d = DagBag();'  
  
Airflow can be a bit tricky to setup.  
- Do you have the airflow scheduler running?  
- Do you have the airflow webserver running?  
- Have you checked that all DAGs you want to run are set to On in the web ui  
- Do all the DAGs you want to run have a start date which is in the past?  
- Do all the DAGs you want to run have a proper schedule which is shown in the web ui?  
- If nothing else works, you can use the web ui to click on the dag, then on Graph View. Now select the first task and click on Task Instance. In the paragraph Task Instance Details you will see why a DAG is waiting or not running.

```
