# author: Victor Kofia 
# email: victor@caresense.com

from datetime import datetime
from datetime import timedelta

import airflow
from airflow.models import DAG
from airflow.hooks.mysql_hook import MySqlHook
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator

args = {
    'owner': 'vkofia',
    'start_date': airflow.utils.dates.days_ago(2)
}

dag = DAG(
    dag_id='refresh_ecaa_table',
    default_args=args,
    schedule_interval='0 * * * *',
    dagrun_timeout=timedelta(minutes=60),
)

def update_existing_records(*args, **kwargs):
    conn = MySqlHook(mysql_conn_id='mysql_default', delegate_to=None)
    # df = conn.get_pandas_df(sql=query)
    engine = conn.get_sqlalchemy_engine()
    connection = engine.connect()
    trans = connection.begin()
    result = connection.execute("""
    /* Update records that have been modified */
    UPDATE `CareSenseCustom`.`ECAABigTable` JOIN `CareSenseData`.`BigTable` ON `ECAABigTable`.`PK` = `BigTable`.`PK`
    SET `ECAABigTable`.`SessionID` = `BigTable`.`SessionID`,
        `ECAABigTable`.`Status` = `BigTable`.`Status`,
        `ECAABigTable`.`Owner` = `BigTable`.`Owner`,
        `ECAABigTable`.`EntryDate` = `BigTable`.`EntryDate`,
        `ECAABigTable`.`PatientID` = `BigTable`.`PatientID`,
        `ECAABigTable`.`Privileges` = `BigTable`.`Privileges`,
        `ECAABigTable`.`TreatmentID` = `BigTable`.`TreatmentID`,
        `ECAABigTable`.`Anatomy` = `BigTable`.`Anatomy`,
        `ECAABigTable`.`DaysElapsed` = `BigTable`.`DaysElapsed`,
        `ECAABigTable`.`StandardET` = `BigTable`.`StandardET`,
        `ECAABigTable`.`Dynamic` = `BigTable`.`Dynamic`,
        `ECAABigTable`.`Category` = `BigTable`.`Category`,
        `ECAABigTable`.`Question` = `BigTable`.`Question`,
        `ECAABigTable`.`Answer` = `BigTable`.`Answer`,
        `ECAABigTable`.`Tag` = `BigTable`.`Tag`,
        `ECAABigTable`.`Modified` = `BigTable`.`Modified`
    /* Switch to modified in < hour ago */
    WHERE `BigTable`.`Modified` <> `ECAABigTable`.`Modified`;
    """)
    trans.commit()
    connection.close()
    return 'Updated rows: ' + str(result.rowcount)


def add_new_records(*args, **kwargs):
    conn = MySqlHook(mysql_conn_id='mysql_default', delegate_to=None)
    # df = conn.get_pandas_df(sql=query)
    engine = conn.get_sqlalchemy_engine()
    connection = engine.connect()
    trans = connection.begin()
    result = connection.execute("""
    /* Add new records */
    INSERT INTO `CareSenseCustom`.`ECAABigTable`
    SELECT *
    FROM `CareSenseData`.`BigTable` 
    WHERE `BigTable`.`Owner` = 'EastCarolinaAnesthesia'
    AND `BigTable`.`Status` IN (4, 7)
    AND `BigTable`.`EntryDate` > DATE_SUB(NOW(), INTERVAL 1 DAY)
    AND `BigTable`.`PK` NOT IN (SELECT `PK` FROM `CareSenseCustom`.`ECAABigTable`);
    """)
    trans.commit()
    connection.close()
    return 'Inserted rows: ' + str(result.rowcount)

start = DummyOperator(task_id='start', dag=dag)
update_existing = PythonOperator(task_id='update_existing', python_callable=update_existing_records, dag=dag)
add_new = PythonOperator(task_id='add_new', python_callable=add_new_records, dag=dag)
end = DummyOperator(task_id='end', dag=dag)

start >> update_existing >> end
start >> add_new >> end
